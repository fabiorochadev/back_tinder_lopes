package br.com.lopesdigital.backtinder.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class LocationDto (
    val state: String,

    @JsonProperty("placeId")
    val placeID: String,
    val placeDescription: String
)