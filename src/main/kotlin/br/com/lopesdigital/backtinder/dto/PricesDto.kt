package br.com.lopesdigital.backtinder.dto

data class PricesDto (
    val min: String,
    val max: String
)