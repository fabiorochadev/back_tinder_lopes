package br.com.lopesdigital.backtinder.repository

import br.com.lopesdigital.backtinder.model.UserData
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface IUserDataRepository : MongoRepository<UserData,ObjectId> {
    fun findByUserId(id: String): UserData
}