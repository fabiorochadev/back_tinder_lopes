package br.com.lopesdigital.backtinder.controller

import br.com.lopesdigital.backtinder.dto.UserDataDto
import br.com.lopesdigital.backtinder.model.UserData
import br.com.lopesdigital.backtinder.service.UserDataService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/profile")
class ProfileController (
    private val  userDataService: UserDataService
){
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody request: UserDataDto) {
        userDataService.create(request)
    }

    @GetMapping("/{id}")
    fun findByUserId( @PathVariable id: String): ResponseEntity<UserData> {
        val  userDatas = userDataService.findByUserId(id)
        return ResponseEntity.ok(userDatas)
    }

    @GetMapping
    fun findAllUserData(): ResponseEntity<List<UserData>> {
        val userDatas = userDataService.findAll()
        return ResponseEntity
            .ok(
                userDatas
            )
    }

}