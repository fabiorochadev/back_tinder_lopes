package br.com.lopesdigital.backtinder

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@SpringBootApplication
@ComponentScan("br.com.lopesdigital.backtinder")
@EntityScan("br.com.lopesdigital.backtinder.model")
@EnableMongoRepositories("br.com.lopesdigital.backtinder.repository")
class BackTinderLopesApplication

fun main(args: Array<String>) {
	runApplication<BackTinderLopesApplication>(*args)
}
