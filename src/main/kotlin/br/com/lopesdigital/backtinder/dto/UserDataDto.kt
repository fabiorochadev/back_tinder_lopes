package br.com.lopesdigital.backtinder.dto

data class UserDataDto(val liked: List<String>, val disliked: List<String>, val profile: ProfileDto, val userId: String)
