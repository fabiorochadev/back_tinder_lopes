package br.com.lopesdigital.backtinder.model


import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document("userData")
class UserData (@Id val id: ObjectId, val liked: List<String>, val disliked: List<String>, @Field("profile")val profile: Profile, val userId: String)