package br.com.lopesdigital.backtinder.service

import br.com.lopesdigital.backtinder.dto.UserDataDto
import br.com.lopesdigital.backtinder.model.Location
import br.com.lopesdigital.backtinder.model.Prices
import br.com.lopesdigital.backtinder.model.Profile
import br.com.lopesdigital.backtinder.model.UserData
import br.com.lopesdigital.backtinder.repository.IUserDataRepository
import org.bson.types.ObjectId
import org.springframework.stereotype.Service

@Service
class UserDataService( private val userDataRepository: IUserDataRepository) {
    fun create(userDataDto: UserDataDto): UserData = userDataRepository.save(
        UserData(
            userId = userDataDto.userId,
            liked = userDataDto.liked,
            disliked = userDataDto.disliked,
            id = ObjectId.get(),
            profile = Profile(
                distance = userDataDto.profile.distance,
                type = userDataDto.profile.type,
                prices = Prices(max = userDataDto.profile.prices.max, min = userDataDto.profile.prices.min),
                rooms = userDataDto.profile.rooms,
                suits = userDataDto.profile.suits,
                bathrooms = userDataDto.profile.bathrooms,
                parking = userDataDto.profile.parking,
                features = userDataDto.profile.features,
                location = Location(
                    state = userDataDto.profile.location.state,
                    placeID = userDataDto.profile.location.placeID,
                    placeDescription = userDataDto.profile.location.placeDescription
                )

            )
        )
    )

    fun findByUserId(userId: String) : UserData = userDataRepository.findByUserId(userId)
    fun findAll(): List<UserData> = userDataRepository.findAll()

}