# Start with a base image containing Java runtime (mine java 8)
FROM openjdk:11-jre-slim
# Add Maintainer Info
# Add a volume pointing to /tmp
VOLUME /tmp
# Make port 8080 available to the world outside this container
EXPOSE 8080
# The application's jar file (when packaged)
ARG JAR_FILE=target/back_tinder_lopes-0.0.1-SNAPSHOT.jar
# Add the application's jar to the container
ADD ${JAR_FILE} app.jar
# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
#ENTRYPOINT ["java", "-javaagent:/elastic-apm-agent-1.23.0.jar", "-Delastic.apm.capture_headers=true" , "-Delastic.apm.service_name=back-portal-home", "-Delastic.apm.server_url=http://apm-server-apm-server.monitoring.svc.cluster.local:8200", "-Delastic.apm.application_packages=br.com.lopesdigital", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]

