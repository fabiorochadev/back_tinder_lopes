package br.com.lopesdigital.backtinder.model

class Profile(
    val location: Location,
    val distance: String,
    val type: String,
    val prices: Prices,
    val rooms: String,
    val suits: String,
    val bathrooms: String,
    val parking: String,
    val features: List<String>
) {

}