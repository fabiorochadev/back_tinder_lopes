package br.com.lopesdigital.backtinder.dto

data class ProfileDto(
    val location: LocationDto,
    val distance: String,
    val type: String,
    val prices: PricesDto,
    val rooms: String,
    val suits: String,
    val bathrooms: String,
    val parking: String,
    val features: List<String>
)

