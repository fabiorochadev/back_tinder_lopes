package br.com.lopesdigital.backtinder.model

class Location (
    val state: String,
    val placeID: String,
    val placeDescription: String
) {
}